<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\model\Users;
class UsersController extends Controller
{
    public function index(){
        return Users::all();
    }
    public function store(Request $request){
        $client = new Users();
        $client->fill($request->toArray())->save();
		return $client;
    }
    public function destroy($id){
        $client=Users::find($id);
        $client->delete();
    }
    public function show($id){
        $client=Users::find($id);
        return $client;
    }
    public function update($id,Request $request){
        $client=$this->show($id);
        $client->fill($request->all())->save();
        return $client;
    }
  
}
