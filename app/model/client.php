<?php

namespace app\model;

use Illuminate\Database\Eloquent\Model;

class client extends Model
{
    protected $table='client';
    protected $primaryKey="id_client";
    public $timestamps=true;
    const created_at = 'create_ad';
    const update_at = 'update_ad';
    protected $fillable =[
        'CI',
        'name',
        'lastname',
        'address',
        'city',
        'mail',
        'phone',
        'created_ad',
        'updated_ad'
    ];
}
