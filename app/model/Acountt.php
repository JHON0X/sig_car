<?php

namespace app;

use Illuminate\Database\Eloquent\Model;

class Acountt extends Model
{
    protected $table='acount';
    protected $primaryKey="id_acount";
    public $timestamps=true;
    const created_at = 'created_ad';
    const update_at = 'updated_ad';
    protected $fillable = [
        'first_name',
        'last_name',
        'created_ad',
        'updated_ad'
    ];
}
