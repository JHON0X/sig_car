@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="pricw_per_car" class="col-md-4 col-form-label text-md-right">{{ __('pricw_per_car') }}</label>

                            <div class="col-md-6">
                                <input id="pricw_per_car" type="text" class="form-control @error('pricw_per_car') is-invalid @enderror"
                                name="pricw_per_car" value="{{ old('pricw_per_car') }}" required autocomplete="pricw_per_car" autofocus>

                                @error('pricw_per_car')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="quantity" class="col-md-4 col-form-label text-md-right">{{ __('quantity') }}</label>

                            <div class="col-md-6">
                                <input id="quantity" type="text" class="form-control @error('quantity') is-invalid @enderror"
                                name="quantity" value="{{ old('quantity') }}" required autocomplete="quantity" autofocus>

                                @error('quantity')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="order_date" class="col-md-4 col-form-label text-md-right">{{ __('order_date') }}</label>

                            <div class="col-md-6">
                                <input id="order_date" type="text" class="form-control @error('order_date') is-invalid @enderror"
                                name="order_date" value="{{ old('order_date') }}" required autocomplete="order_date" autofocus>

                                @error('order_date')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="return_date" class="col-md-4 col-form-label text-md-right">{{ __('return_date') }}</label>

                            <div class="col-md-6">
                                <input id="return_date" type="text" class="form-control @error('return_date') is-invalid @enderror"
                                name="return_date" value="{{ old('return_date') }}" required autocomplete="return_date" autofocus>

                                @error('return_date')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="days" class="col-md-4 col-form-label text-md-right">{{ __('days') }}</label>

                            <div class="col-md-6">
                                <input id="days" type="text" class="form-control @error('days') is-invalid @enderror"
                                name="days" value="{{ old('days') }}" required autocomplete="days" autofocus>

                                @error('days')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="namber_orders" class="col-md-4 col-form-label text-md-right">{{ __('namber_orders') }}</label>

                            <div class="col-md-6">
                                <input id="namber_orders" type="text" class="form-control @error('namber_orders') is-invalid @enderror"
                                name="namber_orders" value="{{ old('namber_orders') }}" required autocomplete="namber_orders" autofocus>

                                @error('namber_orders')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
